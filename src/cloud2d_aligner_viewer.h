#pragma once

#include <srrg_core_viewers/simple_viewer.h>
#include "cloud2d.h"
#include "cloud2d_aligner.h"
#include "cloud2d_trajectory.h"
namespace srrg_scan_matcher_gui {
  using namespace std;
  using namespace Eigen;
  using namespace srrg_scan_matcher;
  using namespace srrg_core;

  class Cloud2DAlignerViewer: public srrg_core_viewers::SimpleViewer{
  public:
    Cloud2DAlignerViewer(){
      _reference = 0;
      _current = 0;
    }

    inline void setReference(Cloud2DWithTrajectory* reference){_reference=reference;}
    inline void setCurrent(Cloud2DWithTrajectory* current){_current=current;}
    inline void setCorrespondences(CorrespondenceVector& correspondences){_correspondences=correspondences;}

    inline CorrespondenceVector& correspondences(){ return _correspondences;}

    void draw();
    void clear();
    
  protected:
    void drawCorrespondences(Eigen::Isometry2f& T);
    Cloud2DWithTrajectory* _reference;
    Cloud2DWithTrajectory* _current;
    CorrespondenceVector _correspondences;
  };

}
