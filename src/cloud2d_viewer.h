#pragma once
#include <qevent.h>
#include <vector>
#include <srrg_core_viewers/simple_viewer.h>
#include "cloud2d.h"
#include "cloud2d_trajectory.h"

namespace srrg_scan_matcher_gui {

  class Cloud2DViewer: public srrg_core_viewers::SimpleViewer {
  public:
    Cloud2DViewer();
    void draw();
    void clear();
    inline void drawNormals(bool draw_normals){_draw_normals = draw_normals;};
    std::vector<srrg_scan_matcher::Cloud2DWithPose*> clouds;
  protected:
    bool _draw_normals;
  };


}
